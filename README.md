## steps

```
kubectl create sa tigera-admin-team
kubectl create sa tigera-platform-team
kubectl create sa tigera-security-team
kubectl create sa tigera-developer-team
kubectl create sa tigera-readonly-team
```


```
kubectl create clusterrolebinding tigera-admin-team-access --clusterrole tigera-network-admin --serviceaccount default:tigera-admin-team
kubectl create clusterrolebinding tigera-platform-team-access --clusterrole tigera-plaform-team --serviceaccount default:tigera-platform-team
kubectl create clusterrolebinding tigera-security-team-access --clusterrole tigera-security-team --serviceaccount default:tigera-security-team
kubectl create clusterrolebinding tigera-developer-team-access --clusterrole tigera-developer-team --serviceaccount default:tigera-developer-team
kubectl create clusterrolebinding tigera-readonly-team-access --clusterrole tigera-readonly-team --serviceaccount default:tigera-readonly-team
```
